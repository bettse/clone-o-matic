# Clone-o-matic

Clone Mifare Classic cards to gen1a

## Hardware

 * Runs on a raspberry pi
 * 2 acr122u
 * Epson TM-T20II receipt printer
 * Blink1

## Software

 * Python 3
 * python libraries in requirements.txt

### Service

1. `sudo cp clone-o-matic.service /lib/systemd/system/`
2. `sudo chmod 644 /lib/systemd/system/clone-o-matic.service`
3. `sudo systemctl daemon-reload`
4. `sudo systemctl enable clone-o-matic.service`
5. `sudo systemctl start clone-o-matic.service`

`journalctl --since today -f -u clone-o-matic.service`

