#!/usr/bin/python3

import os
import time
import logging
import errno
import sys
import subprocess
from subprocess import TimeoutExpired
import base64
import gzip
import threading
from datetime import datetime

from pynfc import Nfc, Mifare, TimeoutException
from escpos.printer import Usb
from blink1.blink1 import Blink1

#log = logging.getLogger(__name__)
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

class Cloner:
    def __init__(self):
        self.printer = Usb(0x04b8, 0x0e15)
        self.blinky = Blink1()

    def color(self, color, fade=0):
        self.blinky.fade_to_color(fade, color)

    def find_devices(self):
        env={"LIBNFC_AUTO_SCAN": "true", "LIBNFC_DEVICE": "acr122_usb"}
        result = subprocess.run(['nfc-scan-device'], stdout=subprocess.PIPE, env=env)
        lines = [x.strip() for x in result.stdout.decode().split("\n")]
        candidates = list(filter(lambda line: line.startswith("acr122_usb:"), lines))
        if len(candidates) != 2:
            raise Exception("Incorrect number of readers")
        return candidates

    def handle_mfc(self, left, right, uid):
        try:
            self.printer.text(datetime.now().isoformat() + "\n")
            self.printer.barcode(uid.decode().upper(), 'CODE39')

            result = subprocess.run(['nfc-list'], stdout=subprocess.PIPE, env={"LIBNFC_DEVICE": left})
            print("return code %s" % result.returncode)
            self.printer.text(result.stdout.decode() + "\n")

            if result.returncode != 0:
                return
            self.color('yellow')

            if self.run_mfoc(uid, left) != 0:
                return
            self.color('purple')

            data = open(uid, "rb").read()
            s_out = gzip.compress(data)
            encoded = base64.b64encode(s_out)
            self.printer.text("Base64 encoded gzip of card binary\n")
            self.printer.qr(encoded, size=8)
            self.printer.text("to undo: `echo -n <base64> | base64 -d | gunzip > tag.bin`\n")
            self.color('orange')

            self.printer.text("nfc-mfclassic w a u temp\n")
            result = subprocess.run(['nfc-mfclassic', 'W', 'a', 'u', uid, 'v'], stdout=subprocess.PIPE, env={"LIBNFC_DEVICE": right})
            print(result.stdout.decode())
            print("return code %s" % result.returncode)
            if result.returncode != 0:
                self.printer.text(result.stdout.decode() + "\n")
                return
            self.color('gold')
            self.printer.text("CLONE SUCESSFUL\n")

        except:
            print("Exception in handle_mfc")
            print(sys.exc_info()[0])
            self.color('red')
        finally:
            self.blinky.off()
            self.printer.cut()

    def output_reader(self, proc):
        for line in iter(proc.stdout.readline, b''):
            self.color('yellow')
            self.color('black', 1000)
            print('got line: {0}'.format(line.decode('utf-8')), end='')

    def run_mfoc(self, uid, left):
        timeout = 90
        self.printer.text("Starting mfoc with %ss timeout\n" % timeout)
        proc = subprocess.Popen(['/usr/local/bin/mfoc', '-k', '2A2C13CC242A', '-O', uid], stdout=subprocess.PIPE, env={"LIBNFC_DEVICE": left})
        t = threading.Thread(target=self.output_reader, args=(proc,))
        t.start()
        try:
            return proc.wait(timeout=timeout)
        except TimeoutExpired:
            self.printer.text('mfoc timeout')
            proc.kill()
        t.join()
        return proc.returncode

    def run(self):
        print("Start")
        #TODO: move to __init__
        [left, right] = self.find_devices()
        print('left: %s\tright: %s' % (left, right))
        n = Nfc(left)
        last_uid = None
        # print(n.pdevice.contents.connstring)
        n.pctx.contents.allow_autoscan = False

        try:
            while True:
                self.color('white')
                print("Waiting for tag")
                if n is None:
                    n = Nfc(left)
                for target in n.poll():
                    try:
                        if type(target) == Mifare:
                            if target.uid != last_uid:
                                self.color('green')
                                last_uid = target.uid
                                n = None
                                self.handle_mfc(left, right, target.uid)
                            else:
                                self.color('blue')
                        else:
                            self.color('red')

                    except TimeoutException:
                        print("TimeoutException")
                        pass
        except KeyboardInterrupt:
            n = None
            pass



if __name__ == "__main__":
    if os.geteuid() == 0:
        cloner = Cloner()
        cloner.run()
    else:
        print("must be root")
